// Introducing vectors; more on functions.

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector> /* <--- new stuff */
using std::vector;

int main()
{
	/* problem:  read a list of strings from standard input
	 * and then print the list back out in reverse order.
	 * The issue is that we don't know ahead of time how
	 * many variables we'll need...  Vectors are one answer.
	 * For convenience, we'll terminate our list of sstrings with
	 * the string "END". */
	string s;
	vector<string> v;
	while(cin >> s && s != "END") {
		v.push_back(s);
	}
	#if 0
	/* NOTE: you have to be careful when using a size_t counter
	 * and i-- as the update, else you may encounter an infinite loop. */
	for (size_t i = v.size()-1; i != (size_t)-1; i--) {
		cout << v[i] << endl;
	}
	/* TODO: use the above version, but change the i != -1 to be i >= 0.
	 * Take note of the error messages, and be sure you understand what
	 * went wrong. */
	#endif
	for (size_t i = 0; i < v.size(); i++) {
		cout << v[v.size()-1-i] << endl;
	}

	/* exercise: read two lists of strings from standard input
	 * and then print them out shuffled together (alternate printing
	 * from the first list, and then the second...). NOTE: the two
	 * lists do not have to be the same length. */
	/* step 1: read into two vectors: */
	vector<string> v1;
	vector<string> v2;
	while(cin >> s && s != "END") {
		v1.push_back(s);
	}
	while(cin >> s && s != "END") {
		v2.push_back(s);
	}
	/* now the shuffling.  first, take care of the
	 * case when BOTH lists have data in them: */
	for (size_t i = 0; i < v1.size() && i < v2.size(); i++) {
		cout << v1[i] << endl << v2[i] << endl;
	}
	if (v1.size() > v2.size()) {
		for (size_t i = v2.size(); i < v1.size(); i++) {
			cout << v1[i] << endl;
		}
	} else {
		for (size_t i = v1.size(); i < v2.size(); i++) {
			cout << v2[i] << endl;
		}
	}
	/* TODO: the if()...else is actually irrelevant!  Rewrite + 
	 * simplify this by just keeping the for loops inside. */
	/* TODO: once you're done, convert what you wrote into a *function* that
	 * takes two vectors of strings, and returns a vector with the strings
	 * shuffled.  Remember: you have to write it OUTSIDE of main()... */
	/* TODO: write test code for the function you just wrote. */

	/* TODO: write code that *intentionally* accesses vector elements
	 * which are out of bounds.  E.g., try to access V[15] when the
	 * vector only has 10 items.  Nothing will prevent this from compiling,
	 * but how far out of bounds do you have to go before your program
	 * crashes?  When it does crash, take note of the error messages;
	 * this will unfortunately not be the last time you see them : )  */
	return 0;
}
