#include <iostream>
using std::cin;
using std::cout;
using std::endl;

/* Functions give us yet another way to control the flow of execution of the
 * program. E.g., main() is a function that we've seen over and over... */

/* syntax:
 * datatype functionname(parameterlist) { c++ statements...}
 * think of f(x) from your calculus days... remember this notation?
 * f:R ---> R
 * Breaking it down, it says there is a function named f, which has domain R
 * and range R.  The C++ equivalent would be something like:
 * double f(double);
 * NOTE: thing above is called a "function prototype"
 */

/* example: write down a c++ version of
 * the polynomial f(x) = x^2 + 1 */
double p(double x) {
	return x*x + 1;
}
/* NOTE: see the main() function below for how to use this. */

/* Terminology:
 * formal parameter -- this is the variable that is used
 * in the function's parameter list.
 * actual parameter -- this is the variable / value that you
 * actually called the function with.
 * E.g., in the above function, x was the formal parameter.
 * If I called the function via p(a), then a is the actual parameter.
 *
 * For a call by value parameter, the formal parameter is a *copy* of
 * the actual parameter (formal param and actual param live in different
 * memory locations).
 * For a call by reference parameter, the formal parameter is a *synonym*
 * for the actual parameter (formal and actual params refer to the same
 * piece of memory).
 * */

/* Call by value vs by reference examples follow.  Note that the default in
 * C++ is to have all parameters *by value* unless otherwise specified. */

int fByVal(int x) {
	x = 99;
	return 42;
}

int fByRef(int& x) {
	x = 99;
	return 42;
}

int main(int argc, char *argv[])
{
	/* here's how to call a function: */
	cout << "enter the input: ";
	double number;
	cin >> number;
	cout << "p(" << number << ") = " << p(number) << endl;
	/* more nomenclature: the "number" variable is
	 * refered to as an *actual parameter* */

	int i = 7;
	cout << "i == " << i << endl;
	cout << fByVal(i) << endl;
	cout << "i == " << i << endl;
	cout << fByRef(i) << endl;
	cout << "i == " << i << endl;
	return 0;
}

/* TODO: write a function that takes 3 integers and returns maximal value */
/* TODO: write a function that takes 3 doubles and returns the average value */
/* TODO: finish your homework, and convert that into a function called isPrime,
 * which takes an integer and returns a boolean value indicating whether or not
 * it is prime.
 * */

/* TODO: write a function that takes two integer parameters and swaps the
 * contents, i.e., if x=2 and y=5, then after calling swap(x,y), y=2 and x=5 */

/* TODO: write a function that takes an integer n and returns the
 * nth term in the Fibonacci sequence. */

