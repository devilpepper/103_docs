#pragma once

/* simple list node class.  Just a data element and a pointer
 * to the next node. */
class listNode
{
public:
	int data;
	listNode* next;
};

class list
{
public:
	list(); /* default constructor */
	list(const list& L); /* copy constructor */
	list& operator=(const list& RHS);
	~list(); /* destructor */

	void insertAtFront(int x);
private:
	listNode* root;
};

// TODO: think about the "class invariant" for the list class.
// what guarantees do we need regarding the root, and what the
// root points to upon entering any member function?
