#include<iostream>
using std::cin;
using std::cout;
using std::endl;

// what's the output of printStuff(3)?
void printStuff(unsigned long x)
{
	if(x == 0)
		cout << "We stopped at x == 0\n";
	else {
		printStuff(x-1);
		cout << "Hello from printStuff(" << x << ")\n";
		// TODO: swap the two lines above, and take note of the difference
		// in the output that's produced.
	}
}

// TODO: write a recursive function that prints the base 10 digits of n
// vertically to cout.  for example, printVertically(2358) would print
// 2
// 3
// 5
// 8
// Rules: you can't use any loops.  You can't use vectors or arrays.
// Just let the recursive function calls do the work for you.
void printVertically(unsigned long n)
{
	// your code goes here.
}

// compute a^b
unsigned long power(unsigned long a, unsigned long b)
{
	/* base case: b == 0 */
	if (b==0) return 1;
	/* now think: if I had a magic solution for
	 * b-1, how could I use said solution to solve
	 * the input b? */
	/* a^5 = aaaaa
	 * a^4 = (aaaa)
	 * so... a^b  = a(a^(b-1))
	 *                ^^^^^^
	 *            recursive call */
	return a*power(a,b-1);
}

/* binary search. */

/* TODO: erase this function and write it on your
 * own from scratch.  (Don't worry --  it will still
 * be in git). Alternatively, just rename this and
 * write your own version.  But don't cheat!  */
bool search(int* A, int size, int x)
{
	/* base case: */
	if (size < 1) /* array is empty */
		return false;
	/* now do the recursive breakdown: solve the big problem
	 * in terms of magic recursive solutions to smaller problems */
	int mid = size / 2; /* pick mid-point to check against */
	/* NOTE:  the subarrays will always be STRICTLY smaller than size. */
	if (x > A[mid])
		/* search inside the subarray mid+1 .. size */
		return search(A+mid+1, size - mid - 1, x);
	/* NOTE: using pointer arithmetic to get the subarray */
	else if (A[mid] > x)
		/* search inside subarray 0 .. mid - 1 */
		return search(A,mid,x);
	else /* A[mid] == x */
		return true;
}

int main()
{
	// test out the functions...
	printStuff(3);
	return 0;
	printVertically(3227);
	cout << "2^5 = " << power(2,5) << endl;
	/* binary search test: */
	int A[100];
	for (size_t i = 0; i < 100; i++) {
		A[i] = i*i;
	}
	int x;
	while (cin >> x)
		cout << search(A,100,x) << endl;
	return 0;
}
