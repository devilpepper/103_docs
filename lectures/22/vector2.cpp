/* implementation file for our vector class. */
#include "vector2.h"

/* NOTE: the following would disable assertions: */
// #define NDEBUG 1
/* Note also that this disables assertions at the
 * *preprocessor level*, so that the checks will not leave
 * a trace in the compiled binary (so, no performance cost).
 * Note also that this is much better done from a makefile
 * with -D passed to gcc.  I'm putting it here so it is
 * easier for you to mess with. */

#include <cassert> /* for assert() */

vector2::vector2(size_t initSize)
{
	/* must establish class invariant. */
	this->data = new int[initSize];
	this->size = 0;
	this->capacity = initSize;
}

vector2::~vector2() // destructor.  <--- NEW STUFF!
{
	/* called automatically whenever an instance:
	 * (a) goes out of scope, or
	 * (b) is explicitly deleted */
	delete[] this->data;
}

void vector2::push_back(int x)
{
	if(size == capacity) {
		/* how much space to add?  Consider two approaches:
		 * 1. double the size when we run out
		 * 2. add 10 elements when we run out
		 * TODO: determine the cost of inserting n items
		 * into an empty vector for each approach.
		 * */
		int* temp = new int[2*this->capacity];
		for (size_t i = 0; i < this->size; i++) {
			temp[i] = this->data[i];
		}
		this->capacity *= 2;
		delete[] this->data;
		this->data = temp;
	}
	/* if we had space: */
	data[size++] = x;
}

int vector2::pop_back()
{
	/* need to return data[size-1],
	 * and also --size, so that this element is
	 * effectively removed. */
	assert(size>0);
	/* TODO: try to pop_back from an empty vector and see what
	 * assert does for you. */
	/* TODO: Uncomment the NDEBUG line above (or compile with
	 * -DNDEBUG) and see if/when/how your program actually breaks.
	 *  */
	return this->data[--size];
}

/* NOTE: we are returning *by reference* rather than
 * by value.  This is done so that things like
 *    v[0] = 99;
 * will work as expected.  Normally temporary storage would be
 * allocated for the return value (often in a register for
 * integer types) and it would make no sense to assign a value
 * to it.  When returning by reference, you are prescribing the
 * actual piece of memory for the return value.  So when we
 * return data[i] below, we are actually making v[i] a synonym
 * for data[i];
 * */
int& vector2::operator[](size_t i)
{
	assert(i < size); /* make sure access is in bounds. */
	return data[i];
}
