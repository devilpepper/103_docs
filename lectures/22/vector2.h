/*
 * We'll try to implement our own vector class, and see what is
 * going on under the hood.  What you should be learning from all this:
 * 1. more about classes
 * 2. dynamic memory management
 */
#pragma once

#include <cstddef> // for size_t definition.

class vector2 {
	public:
	/* stuff in this section is declared as "public" which means that it is
	 * part of the public interface -- i.e., the part of our code that other
	 * programmers are supposed to use. */

	/* constructors: */
	vector2(size_t initSize=64); // default constructor
	/* could call via vector2(somevalue) or just vector2(); */
	/* destructors:  <--- NEW STUFF! */
	~vector2(); /* note the syntax: no return value; no params; same name. */
	/* member functions: */
	void push_back(int x);
	int pop_back();
	int& operator[](size_t i);

	private:
	/* stuff below this line is declared as "private" meaning that these
	 * members are considered to be implementation details.  No user of our
	 * class should be concerned with it.  The compiler will in fact prevent
	 * non-member functions from accessing any of these members, but even if it
	 * didn't, these pieces are considered subject to change without notice,
	 * and making manual modifications might break the class invariant,
	 * causing all sorts of problems.
	 * */

	/* How to represent a vector with only primitive types and arrays?
	 * what data do we need to store? */
	int* data; // storage for vector.  we'll allocate this in the constructor
	size_t size; // data[0...size-1] are the vector's contents
	size_t capacity; // stores the actual size of the underlying data array
};
