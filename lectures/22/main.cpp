#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include "vector2.h"

int example(vector2 V)
{
	V.push_back(199);
	V.push_back(2929);
	cout << V[0] << endl;
	return 0;
}

int main()
{
	vector2 v;
	v.push_back(199);
	v.push_back(2929);
	cout << v[0] << endl << v[1] << endl;
	/* TODO: uncomment the following line which calls the example
	 * function above, and see if you can figure out what is going
	 * wrong.  It's kind of tricky. */
	// cout << example(v) << endl;
	return 0;
}
