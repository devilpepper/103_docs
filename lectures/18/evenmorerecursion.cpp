#include<iostream>
using std::cin;
using std::cout;
using std::endl;
#include <vector>
using std::vector;
#include <cstdio> /* for printf */
#include <cstdlib> /* for rand() */

unsigned long xgcd(unsigned long a, unsigned long b, long& u, long& v)
{
	if (a % b == 0) {
		u = 0;
		v = 1;
		return b;
		/* note that gcd(a,b) = b = u*a + v*b */
	}
	long uu,vv; /* u' and v' from the notes */
	int d = xgcd(b,a%b,uu,vv);
	/* gcd(a,b) == d == uu*b + vv*r, (r = a%b) */
	/* a little algebra shows that u corresponding to (a,b)
	 * should be vv, and v should be uu - vv*(a/b) */
	u = vv;
	v = uu - vv*(a/b);
	return d;
}
/* TODO: delete the body of the above function and make sure you can write
 * it yourself from scratch. */

vector<vector<int> > genPerms(vector<int> L)
{
	/* Write code that return a list of all *permutations* of the list L.
	 * IDEA: make a choice for the last element, and then range through all
	 * permutations of the beginning n-1 elements.  NOTE: this is backwards
	 * from the way I described the algorithm in lecture, but has the
	 * advantage that it is easy to do with plain old vectors (vectors do
	 * not have an efficient version of push_front or pop_front...) */
	vector<vector<int> > R;
	if (L.size() == 0) {
		R.push_back(vector<int>());
		return R;
	}
	int l; /* store last element. */
	for (size_t i = L.size()-1; i != (size_t)-1; i--) {
		/* strip last element, permute the rest, and stick last one back
		 * on the end of each returned list. */
		l = L.back();
		L[L.size()-1] = L[i];
		L[i] = l;
		l = L.back();
		L.pop_back();
		vector<vector<int> > tmp = genPerms(L); /* NOTE: L is now shorter. */
		for (size_t j = 0; j < tmp.size(); j++) {
			tmp[j].push_back(l);
			R.push_back(tmp[j]);
		}
		/* NOTE: L doesn't change during the calls, so it is fine not
		 * to put it back in its original order, as long as we make sure
		 * each item gets a turn at being last. */
		L.push_back(l);
	}
	return R;
}
/* TODO: delete the body of the above function and make sure you can write
 * it yourself from scratch. */

void gcdTest()
{
	unsigned long a,b,d;
	long u,v;
	while(true) {
		cout << "Enter a,b: ";
		cin >> a;
		cin >> b;
		if(a==0 || b==0) return;
		d = xgcd(a,b,u,v);
		printf("gcd(%lu,%lu) = %li*%lu + %li*%lu = %lu\n",a,b,u,a,v,b,d);
	}
}

void permTest()
{
	vector<int> L;
	for (size_t i = 0; i < 4; i++) {
		L.push_back(i);
	}
	vector<vector<int> > P = genPerms(L);
	for (size_t i = 0; i < P.size(); i++) {
		for (size_t j = 0; j < P[i].size(); j++) {
			cout << P[i][j] << " ";
		}
		cout << endl;
	}
}

int main()
{
	permTest();
	gcdTest();
	return 0;
}
