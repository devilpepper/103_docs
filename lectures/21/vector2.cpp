/* implementation file for our vector class. */
#include "vector2.h"

/* this will disable assertions: */
#define NDEBUG 1
/* NOTE: this is much better done from a makefile with -D
 * passed to gcc.  I'm putting it here so it is easier
 * for you to mess with. */

#include <cassert> /* for assert() */

vector2::vector2(size_t initSize)
{
	/* must establish class invariant. */
	this->data = new int[initSize];
	this->size = 0;
	this->capacity = initSize;
}

vector2::~vector2() // destructor.  <--- NEW STUFF!
{
	/* called automatically whenever an instance:
	 * (a) goes out of scope, or
	 * (b) is explicitly deleted */
	delete[] this->data;
}

void vector2::push_back(int x)
{
	/* if we had space: */
	data[size++] = x;
	/* TODO: make this handle the case where you've run out of space.
	 * Use the code from last lecture which expanded an array. */
}

/* TODO: write a corresponding "pop_back" function that removes
 * the last element of the vector. */

int vector2::operator[](size_t i)
{
	/* We'll do this properly next time... for now: */
	return data[i];
}
