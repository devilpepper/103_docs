#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include "vector2.h"

int example()
{
	vector2 V;
	V.push_back(199);
	V.push_back(2929);
	return 0;
} /* at this point, V is going out of scope. ~vector2() will be called. */

int main()
{
	/* TODO: write more test code to see if your vector really works
	 * the way you expect. */
	vector2 v;
	v.push_back(199);
	v.push_back(2929);
	cout << v[0] << endl << v[1] << endl;
	return 0;
}
