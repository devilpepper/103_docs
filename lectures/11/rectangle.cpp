/* rectangle.cpp
 * This is the implementation of our rectangle class.  Here we will
 * define *how* the rectangle does what it does.  (Under the hood stuff.)
 * */
#include "rectangle.h"
#include <iostream>
using std::cout;
using std::endl;

rectangle::rectangle(size_t w, size_t h)
{
	// set our width and heigt from w,h:
	this->width = w;
	this->height = h;
	this->height = h;
	/* NOTE: the "arrow" operator '->'.  This is just
	 * shorthand for '*' combined with '.': */
	/* TODO: comment out the below and see what goes wrong: */
	//*this.width = w;
	/* the problem: dot has higher precedence than '*'.  Could fix
	 * this with some parens: */
	//(*this).width = w;
	/* But this is too annoying to type.  Hence the arrow thing.
	 * "(*pointer).blah" is exactly equivalent to "pointer->blah" */
}

size_t rectangle::area()
{
	// TODO: make this work
	return 0;
}

size_t rectangle::perimeter()
{
	// TODO: make this work
	return 0;
}

bool rectangle::isSquare()
{
	// TODO: make this work
	return false; // statistically speaking this is the right answer...
}

void rectangle::draw()
{
	/* IDEA: we'll try to print the outline of the rectangle
	 * on the terminal with the star character.  Fancy... ~.~
	 * */
	/* as a warm up, we'll forget about scaling to the terminal size
	 * and the fact that characters are usually taller than they
	 * are wide, and just draw in absolute coordinates.
	 * */
	for (size_t i = 0; i < this->width; i++) {
		cout << "*";
	}
	cout << endl;
	for (size_t i = 0; i < this->height - 2; i++) {
		cout << "*";
		for (size_t j = 0; j < this->width - 2; j++) {
			cout << " ";
		}
		cout << "*\n";
	}
	for (size_t i = 0; i < this->width; i++) {
		cout << "*";
	}
	cout << endl;
	/* TODO: re-write this from scratch. */
}
