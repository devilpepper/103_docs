/* strings.cpp
 * More examples working with strings...
 * */
#include <iostream>
using std::cin;
using std::cout; 
using std::endl;
#include <string>
using std::string;

/* TODO:  write a function that takes a string and a character
 * and returns the number of occurences of the character in the string.
 * */
int countChars(const string& s, char c)
{
	return 0;
}

/* TODO: write a function that takes a string and returns a boolean
 * indicating whether or not it was a palindrome.
 * */

/* DONE: write a function that takes two strings and returns an
 * integer value indicating whether or not the first was a substring
 * of the second; the integer should be the index at which the string
 * was found, or -1 to indicate that the string was not found.
 * For example, findSubstr("def", "abcdef") would return 3.
 * */
unsigned long subIndex(string &s1 const, string &s2 const) {
	bool isSubString = false;
	int i, j, l1 = s1.length(), l2 = s2.length();

	for(i=0; (!isSubString && (isSubString = i<((l1-l2)+1))); i++)
		for(j=0; (isSubString && j<l2); j++) isSubString &= (s1[i+j] == s2[j]);
	if(!isSubString) i=-1;
	return i;
}

/* NOTE: the const by reference parameters are used to "fake"
 * a by value call, but with the efficiency of by reference. */
int substring(const string& s1, const string& s2) {
	return -1;
}

/* NOTE: there is a built-in string function for this (find(str,pos)).
 * TODO: try it out.
 * */

/* TODO: write a function that takes a string and returns another
 * string, which is the input reversed.
 * */

int main() {
	/* TODO: write test code for all of the functions you wrote above */
	return 0;
}

