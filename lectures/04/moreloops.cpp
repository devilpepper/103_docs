/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <cstdio> /* needed for printf and friends. */

int main()
{
	/* a slight generalization of an earlier exercise:
	 * for integers n and k, determine the largest power
	 * of k that divides n. */
	cout << "enter two integers: ";
	int n,k;
	cin >> n >> k;
	/* now compute the largest power of k that still divides n */
	/* our process, in English: repeatedly "remove" factors of k from n,
	 * until there are none left; keep the number of removals in count.
	 * "loop invariants": each variable should have a clear
	 * meaning to you.  during each iteration of a loop
	 * you have to make sure this meaning is maintained.
	 * In this example:
	 * count === number of times we have removed a factor of k
	 * n     === original input with count k's removed
	 * k     === input from user -- shouldn't change.
	 * */
	int count = 0;
	while (n % k == 0) { /* that is, while there are still factors of k */
		n /= k; /* reminder: shorthand for n = n/k; */
		count++;
		/* double check: do our variables still have the right meaning
		 * at the end of a loop iteration?
		 * yes :D */
	}
	cout << "largest power was " << k << "^" << count << "\n";

	/* Exercise: read an arbitrary list of integers and then print
	 * the max, min, and mean. */
	/* variables we might need:
	 * storage for minimum, the maximum, the count,
	 * and another for the sum. (we can reuse n for user input...) */
	int min, max, /* reuse count */ sum;
	count = 0;
	/* NOTE: min and max are not well defined at the moment. */
	while (cin >> n) { /* NOTE: we can end input with ctrl-d */
		count++;
		if (count == 1) {
			min = max = sum = n;
			continue; /* moves to next iteration... */
		}
		/* now the general case (we have read at least one number already) */
		if (n < min) /* is min still right? */
			min = n;
		if (n > max) /* is max still right? */
			max = n;
		sum += n;
	}
	printf("min  == %i\nmax == %i\nmean == %f\n",min,max,sum/(double)count);

	/* DONE: write a loop that prints the sum of the first n odd cubes. */
	{
		cout << "how many odd cubes would you like to calculate? ";
		cin >> n;
		count = k = 0;
		while(count <= n && ++k)
		//pretend sum says product. I'm just reusing an old variable
			if((sum = k*k*k)%2 != 0 && ++count) cout << sum << endl;

	}
	/* DONE: write code that gets an integer n from the user and prints out
	 * the n-th term of the fibonacci sequence. */
	{
		cout << "find the nth fibonacci number. Enter n: ";
		cin >> n;
		int temp, f1 = 0, f2 = 1;

		for(k=0; k<n; k++)
		{
			temp = f2;
			f2 += f1;
			f1 = temp;
		}
		cout << "When n = " << n << ", the nth fibonacci number is: " << f1 << endl;
	}

	/* DONE: in addition to the "continue" statement (see above), there
	 * is the "break" statement, which will terminate a loop.  Use the break
	 * statement to write code that checks for whether or not an integer
	 * is a perfect cube (equal to the cube of another integer).  Hint: similarly
	 * to the primality testing idea, you can just "guess and check".  If one of
	 * your guesses works, you quit the loop (hence the break statement). */
	{
		cout << "Enter an integer to see if it is a perfect cube: ";
		cin >> n;
		bool cube = false;
		//pretend sum says product. I'm just reusing an old variable
		for(k=1; (sum = k*k*k)<=n; k++)
			if((n%k == 0) && (cube = (sum == n))) break;
		if(cube) cout << "The cubed root of " << n << " = " << k << endl;
		else cout << n << " is not a perfect cube...\n";
	}
	return 0;
}

/*
enter two integers: 177147 3
largest power was 3^11
12 45 78 96 32 54 12 25 65 75 95 12 8
min  == 8
max == 96
mean == 46.846154
how many odd cubes would you like to calculate? 1
27
125
343
729
1331
2197
3375
4913
find the nth fibonacci number. Enter n: When n = 8, the nth fibonacci number is: 21
Enter an integer to see if it is a perfect cube: The cubed root of 8 = 2
*/
// vim:foldlevel=1
