/* More about loops.
 * See lectures 4,5,6 from Prof. Li.,
 * and chapters 3,4 in the book. */

// the usual stuff:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <cstdio> /* needed for printf and friends. */

int main()
{
	/* a slight generalization of an earlier exercise:
	 * for integers n and k, determine the largest power
	 * of k that divides n. */
	cout << "enter two integers: ";
	int n,k;
	cin >> n >> k;
	/* now compute the largest power of k that still divides n */
	/* our process, in English: repeatedly "remove" factors of k from n,
	 * until there are none left; keep the number of removals in count.
	 * "loop invariants": each variable should have a clear
	 * meaning to you.  during each iteration of a loop
	 * you have to make sure this meaning is maintained.
	 * In this example:
	 * count === number of times we have removed a factor of k
	 * n     === original input with count k's removed
	 * k     === input from user -- shouldn't change.
	 * */
	int count = 0;
	while (n % k == 0) { /* that is, while there are still factors of k */
		n /= k; /* reminder: shorthand for n = n/k; */
		count++;
		/* double check: do our variables still have the right meaning
		 * at the end of a loop iteration?
		 * yes :D */
	}
	cout << "largest power was " << k << "^" << count << "\n";

	/* Exercise: read an arbitrary list of integers and then print
	 * the max, min, and mean. */
	/* variables we might need:
	 * storage for minimum, the maximum, the count,
	 * and another for the sum. (we can reuse n for user input...) */
	int min, max, /* reuse count */ sum;
	count = 0;
	/* NOTE: min and max are not well defined at the moment. */
	while (cin >> n) { /* NOTE: we can end input with ctrl-d */
		count++;
		if (count == 1) {
			min = max = sum = n;
			continue; /* moves to next iteration... */
		}
		/* now the general case (we have read at least one number already) */
		if (n < min) /* is min still right? */
			min = n;
		if (n > max) /* is max still right? */
			max = n;
		sum += n;
	}
	printf("min  == %i\nmax == %i\nmean == %f\n",min,max,sum/(double)count);

	/* TODO: write a loop that prints the sum of the first n odd cubes. */

	/* TODO: write code that gets an integer n from the user and prints out
	 * the n-th term of the fibonacci sequence. */

	/* TODO: in addition to the "continue" statement (see above), there
	 * is the "break" statement, which will terminate a loop.  Use the break
	 * statement to write code that checks for whether or not an integer
	 * is a perfect cube (equal to the cube of another integer).  Hint: similarly
	 * to the primality testing idea, you can just "guess and check".  If one of
	 * your guesses works, you quit the loop (hence the break statement). */

	return 0;
}

// vim:foldlevel=1
