/* Flow of control:
 * Boolean expressions; if and while statements.
 * */

// first, import our usual libraries:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

int main()
{
	/* if statements: execute a piece of code 0 or 1 times,
	 * conditioned upon a boolean expression. */
	cout << "Do you like 103?\n";
	string response;
	getline(cin,response);
	if (response == "yes") { /* NOTE: TWO equals signs. */
		cout << "there's probably something wrong with you.\n";
	} else {
		cout << "congratulations.  you are normal.\n";
	}

	/* here's the general format:
	 * if (<boolean expression>) {
	 *    zero or more statements...
	 * } else if (<boolean expr>) {
	     statements...
	 * } else {
	     statements...
	 * } */
	/* Boolean expressions: expressions that can evaluate to either
	 * true or false.  For example, x == 3, or response != "yes".
	 * other (binary) boolean operators:
	 * ==
	 * !=
	 * <
	 * >
	 * <=
	 * >=
	 * There are also some unary operators, like !
	 * Furthermore, if given two boolean values, we have the
	 * following operators:
	 * && -- this gives the logical "and"
	 * || -- this gives the logical "or"
	 * */

	/* NOTE: any integer can also be interpreted as a boolean expression
	 * using the convention 0 <==> false, anything else <==> true.
	 * */
	cout << "please enter an integer\n";
	int x;
	cin >> x;
	//if (x = 7) {
	if(x ==7){
		cout << "you entered 7.\n";
		/* this will always happen.
		 * DONE: take note of the compiler warning, and fix this.
		 * warning: suggest parentheses around assignment used as truth value [-Wparentheses]*/
	} else {
		cout << "you entered something else.\n";
	}

	/* NOTE: the assignment statement has the value of the RHS; in
	 * this case 7.  So, the cout statement will always happen.
	 * NOTE: there's actually a reason for assignment working this way:
	 * you can do stuff like this:
	 * x = y = z = 7; */

	/* Loops. */
	#if 0
	while (true) {
		cout << "@_@   @_@   @_@   ";
		/* NOTE: If you write an infinite loop like this,
		 * you can break it with Ctrl-C. */
	}
	/* NOTE: the "#if 0 ... #endif" is a convenient way to comment
	 * large blocks of code (although that shouldn't be a very
	 * common occurence...)  */
	#endif
	/* let's do something less ridiculous:
	 * let's print out a table of squares of
	 * the first 20 integers: 1^2=1,2^2=4,3^2=9...
	 * */
	int i=1;
	while (i <= 20) {
		cout << i*i << endl;
		// cout << i^2 << endl; /* NOTE: this does XOR! Not exponentiation! [CURSES!!] */
		i++; /* some equivalents:
				i = i+1;
				i += 1; */
	}
	cout << "i == " << i << endl;

	/* The above pattern of:
	 * 1. initialize some counter kind of variable
	 * 2. loop on the counter being below / above a bound
	 * 3. increment counter at the end of the loop
	 * is very common.  For these kinds of patterns, there is
	 * another loop that may be more suitable: the for loop.  */
	/* here it is to print out the odd squares: */
	for (i = 1; i <= 20; i+=2) {
		cout << i*i << endl;
	}
	cout << "i == " << i << endl; /* DONE: make sure you know why
									 this prints 21 */
	//The loop ended when i was greater than 20. In the last iteration, i = 19
	//19+2 = 21 > 20
	/* DONE: write a piece of code that gets an integer from the user,
	 * and then prints the exponent (i.e. the base 2 logarithm) of
	 * largest power of 2 that goes into that integer.  For example,
	 * since 2 = 2^1, you should print 1.  For 4 = 2^2, the answer
	 * would be 2, for 24 = 2^3*3, the answer would be 3, and for
	 * 20 = 2^2*5, we want 2.  Hint: you should use the % operator,
	 * which gives you the remainder of a division.  */
	{
		int exponent = 0, usr;
		cout << "\n\nEnter an integer to find the largest power of 2 that divides it" << endl;
		cin >> usr;
		//if it was exponenent++, the condition would be evaluated first and
		//then it would be incremented. exponent++ would make exponent
		//never exceed 1
		while(usr % 2 == 0 && ++exponent) usr /= 2;
		cout << exponent << endl;
	}

	/* DONE: write a piece of code that reads integers from standard
	 * input until a negative integer is entered, and then computes
	 * the minimal value, the maximal value, and the average value
	 * of all the (non-negative) integers that were entered.
	 * (Hint: you don't have to store all those numbers at once... )*/
	{//can't use previous usr because my silly code blocks limited the scope of it
		int usr, min, max, sum = 0, divisor = 0;
		cout << "Enter some spaced integers to find their min, max, and average.\n";
		cout << "Enter a negative number to stop entering numbers.\n";
		//3 conditions.
		//1. The input is a valid integer
		//2. The input is not negative
		//3. Increment first then check if not 0. If any of the 
		// 		first 2 conditions are false, ++divisor doesn't happen
		while(cin>>usr && usr>=0 && ++divisor)
		{
			sum += usr;
			if(divisor == 1)
			{
				min = usr;
				max = usr;
			}
			else
			{
				if(usr > max) max = usr;
				if(usr < min) min = usr;
			}
		}
		//Be careful about dividing by 0
		if(divisor == 0) cout << "No positive integers entered" << endl;
		else
		{
			cout << "Min: " << min << "\nMax: " << max << "\nAverage: "
				<< (double)sum/divisor << "\n#s entered: " << divisor << endl;
			//needed to typecast to double to avoid integer division.
			//(sum is being interpreted as a double)
		}
	}

	return 0;
}

/* final output
Do you like 103?
yes
there's probably something wrong with you.
please enter an integer
7
you entered 7.
1
4
9
16
25
36
49
64
81
100
121
144
169
196
225
256
289
324
361
400
i == 21
1
9
25
49
81
121
169
225
289
361
i == 21


Enter an integer to find the largest power of 2 that divides it
2048
11
Enter some spaced integers to find their min, max, and average.
Enter a negative number to stop entering numbers.
7 8 9 45 85 74 12 36 95 -1
Min: 7
Max: 95
Average: 41.2222
#s entered: 9
 
*/

// vim:foldlevel=2
