/* Flow of control:
 * Boolean expressions; if and while statements.
 * */

// first, import our usual libraries:
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;

int main()
{
	/* if statements: execute a piece of code 0 or 1 times,
	 * conditioned upon a boolean expression. */
	cout << "Do you like 103?\n";
	string response;
	getline(cin,response);
	if (response == "yes") { /* NOTE: TWO equals signs. */
		cout << "there's probably something wrong with you.\n";
	} else {
		cout << "congratulations.  you are normal.\n";
	}

	/* here's the general format:
	 * if (<boolean expression>) {
	 *    zero or more statements...
	 * } else if (<boolean expr>) {
	     statements...
	 * } else {
	     statements...
	 * } */
	/* Boolean expressions: expressions that can evaluate to either
	 * true or false.  For example, x == 3, or response != "yes".
	 * other (binary) boolean operators:
	 * ==
	 * !=
	 * <
	 * >
	 * <=
	 * >=
	 * There are also some unary operators, like !
	 * Furthermore, if given two boolean values, we have the
	 * following operators:
	 * && -- this gives the logical "and"
	 * || -- this gives the logical "or"
	 * */

	/* NOTE: any integer can also be interpreted as a boolean expression
	 * using the convention 0 <==> false, anything else <==> true.
	 * */
	cout << "please enter an integer\n";
	int x;
	cin >> x;
	if (x = 7) {
		cout << "you entered 7.\n";
		/* this will always happen.
		 * TODO: take note of the compiler warning, and fix this. */
	} else {
		cout << "you entered something else.\n";
	}

	/* NOTE: the assignment statement has the value of the RHS; in
	 * this case 7.  So, the cout statement will always happen.
	 * NOTE: there's actually a reason for assignment working this way:
	 * you can do stuff like this:
	 * x = y = z = 7; */

	/* Loops. */
	#if 0
	while (true) {
		cout << "@_@   @_@   @_@   ";
		/* NOTE: If you write an infinite loop like this,
		 * you can break it with Ctrl-C. */
	}
	/* NOTE: the "#if 0 ... #endif" is a convenient way to comment
	 * large blocks of code (although that shouldn't be a very
	 * common occurence...)  */
	#endif
	/* let's do something less ridiculous:
	 * let's print out a table of squares of
	 * the first 20 integers: 1^2=1,2^2=4,3^2=9...
	 * */
	int i=1;
	while (i <= 20) {
		cout << i*i << endl;
		// cout << i^2 << endl; /* NOTE: this does XOR! Not exponentiation! */
		i++; /* some equivalents:
				i = i+1;
				i += 1; */
	}
	cout << "i == " << i << endl;

	/* The above pattern of:
	 * 1. initialize some counter kind of variable
	 * 2. loop on the counter being below / above a bound
	 * 3. increment counter at the end of the loop
	 * is very common.  For these kinds of patterns, there is
	 * another loop that may be more suitable: the for loop.  */
	/* here it is to print out the odd squares: */
	for (i = 1; i <= 20; i+=2) {
		cout << i*i << endl;
	}
	cout << "i == " << i << endl; /* TODO: make sure you know why
									 this prints 21 */
	/* TODO: write a piece of code that gets an integer from the user,
	 * and then prints the exponent (i.e. the base 2 logarithm) of
	 * largest power of 2 that goes into that integer.  For exzample,
	 * since 2 = 2^1, you should print 1.  For 4 = 2^2, the answer
	 * would be 2, for 24 = 2^3*3, the answer would be 3, and for
	 * 20 = 2^2*5, we want 2.  Hint: you should use the % operator,
	 * which gives you the remainder of a division.  */

	/* TODO: write a piece of code that reads integers from standard
	 * input until a negative integer is entered, and then computes
	 * the minimal value, the maximal value, and the average value
	 * of all the (non-negative) integers that were entered.
	 * (Hint: you don't have to store all those numbers at once... )*/

	return 0;
}

// vim:foldlevel=2
