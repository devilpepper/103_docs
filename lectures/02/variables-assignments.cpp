// variables-assignments.cpp
#include <iostream>
/* so we don't have to type "std::cout" all the time, we
 * can employ the "using" statement: */
using std::cout;
using std::cin;
/* The "std" is a c++ *namespace*.  Think of these like
 * last names for the symbols in your program:
 * "std::cout" is analogous to "Jones::Bill".  */

#include <string>
using std::string;

#include <cstdio>

int main()
{
	/********* variables *********/
	int x; /* declare a variable to store an integer. */
	// NOTE: at this point, x's value is unpredictable.  It's whatever
	// happened to be left over in main memory.
	/* how to write to that memory? */
	x = 23; // this will store 23 in x.
	// this is "assignment statement": take RHS and store it in LHS
	/* DONE: uncomment the following line,
	 * try to compile; understand messages. */
	//Uncommenting the next line gives error: invalid conversion from ‘const char*’ to ‘int’ [-fpermissive]
	//x = "hello world";

	/* the above statement fails since the datatypes don't match,
	 * however there are some exceptions: */
	cout << "x = " << x << "\n\n";
	double y = 99.999;
	cout << "y = " << y << "\n\n";
	x = y;
	cout << "x = " << x << "\n\n";
	/* notice that a conversion is done automatically,
	 * and that it will round *down* to the nearest integer. */
	cout << "x+y = " << x+y << "\n\n";
	/* notice that the datatype of the expression x+y is double.
	 * If C++ has a choice, it will choose to promote the result
	 * to the larger of the two.  You can extrapolate this to most
	 * of the other arithmetic operators, -,*,/ */
	/* NOTE: integer division might throw you off at first. */
	x = 7;
	cout << "x / 14 = " << x / 14 << "\n";
	cout << "x / 14.0 = " << x / 14.0 << "\n\n";
	/* integer division gives you the quotient, but what
	 * about the remainder??  You can use "mod" or % for this: */
	cout << "x % 4 = " << x % 4 << "\n\n";
	/* DONE: make sure you know the basic datatypes, and what
	 * happens when you mix them in expressions. */

	/* here's a more complex, but very useful data type: the string. */
	string s = "hello world.";
	/* neat things you can do with strings: */
	cout << "number of characters of s: " << s.length() << "\n\n";
	/* there are also nice ways to do concatenation:
	 * http://www.cplusplus.com/reference/string/string/operator+/
	 * and also accessing the individual characters:
	 * http://www.cplusplus.com/reference/string/string/operator[]/
	 
	 * DONE: try them out. */
	{
		s+= " WOOOOO!!";
		cout << s << s[s.length()-1] << "\n\n";
	}
	/* NOTE: the floating point types are *approxmiations* to real numbers,
	 * and indeed, some of the usual algebraic identities don't hold here!!!
	 * For example, consider the cancellation law: a+b = a+c ==> b == c.
	 * This isn't always true with sloppy floating point arithmetic.
	 * 
	 * DONE: try to find an example.  HINT: add a fair-sized integer to a
	 * small floating point value and see what happens.. */
	{
		double a = 9999, b = 1.01, c = 1;
		cout << "a = " << a << " b = " << b << " c = " << c << "\n";
		cout << "a + b = " << a+b << " a + c = " << a+c << " ==> b = c ==> " << b << " = " << c << "...false\n\n";
	}
	
/* DONE: if you're feeling ambitious, try to write a program that reads 5
 * integers and prints out (a) the maximal value, and (b) the average. */
	{ //curly braces are unneccecary. I just wanted to seperate my code from the professor's
		cout << "Enter 5 integers to find out the max and average of them";
		int sum, i, max;
		cin>>max;
		sum = max;
		for(int j=0; j<4; j++) //I can't make myself write the next 3 lines 4 times
		{
			cin>>i;
			if(i>max) max = i;
			sum+=i;
		}
		cout << "Max: " << max << "\nAverage: " << sum/5.0 << "\n";
	}
	return 0;
}

// vim:foldlevel=3
// what?

/* final output
x = 23

y = 99.999

x = 99

x+y = 198.999

x / 14 = 0
x / 14.0 = 0.5

x % 4 = 3

number of characters of s: 12

hello world. WOOOOO!!!

a = 9999 b = 1.01 c = 1
a + b = 10000 a + c = 10000 ==> b = c ==> 1.01 = 1...false

Enter 5 integers to find out the max and average of them 5 7 8 47 32
Max: 47
Average: 19.8
*/ 
