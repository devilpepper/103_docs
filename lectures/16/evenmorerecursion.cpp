#include<iostream>
using std::cin;
using std::cout;
using std::endl;
#include <deque> /* needed for our permutation stuff */
using std::deque;
#include <cstdio> /* for printf */
#include <cstdlib> /* for rand() */

unsigned long xgcd(unsigned long a, unsigned long b, long& u, long& v)
{
	/* TODO: write this.  Our code from lecture covered the plain
	 * gcd algorithm; now modify it to keep track of the linear
	 * combination (i.e., u,v such that gcd(a,b) = ua+vb) */
	u=0; v=1;
	return 1;
}

deque<deque<int> > genPerms(deque<int> L)
{
	/* TODO: write code that return a list of all *permutations* of the list L.
	 * E.g., if the list had 1 2 3 as the elements, then the return value
	 * should be a list of lists containing (not necessarily in this order):
	 * 1 2 3
	 * 1 3 2
	 * 2 1 3
	 * 2 3 1
	 * 3 1 2
	 * 3 2 1
	 * This might be a little challenging.  See the hints below.  */
	/* IDEA: make a choice for the first element,
	 * and then range through all permutations of
	 * the remainder of the list. */
	/* NOTE: I've chosen a deque (double-ended queue) on purpose,
	 * as it provides random access (you can do L[i]), as well as
	 * efficient versions of push_back() *and* push_front(). */
	/* to help you get started, here's the base case: */
	deque<deque<int> > R; // initialize to empty
	if (L.size() == 0) {
		deque<int> empty;
		R.push_front(empty);
		return R;
	}
	return R;
}

/* NOTE: Aux is for storage during the merge. */
void mergeSort(int* A, size_t low, size_t high, int* Aux)
{
	/* base case: */
	if (high <= low) return;
	size_t mid = (high + low) / 2;
	mergeSort(A,0,mid,Aux);
	mergeSort(A,mid+1,high,Aux);
	/* now merge left and right together into Aux,
	 * copy back to A... done 8D */
	/* TODO: make this work.  In fact, maybe even delete the whole thing
	 * and write again from scratch. */
}

/* ~~~~~Test functions~~~~~ */

void gcdTest()
{
	unsigned long a,b,d;
	long u,v;
	while(true) {
		cout << "Enter a,b: ";
		cin >> a;
		cin >> b;
		if(a==0 || b==0) return;
		d = xgcd(a,b,u,v);
		printf("gcd(%lu,%lu) = %li*%lu + %li*%lu = %lu\n",a,b,u,a,v,b,d);
	}
}

void permTest()
{
	deque<int> L;
	for (size_t i = 0; i < 4; i++) {
		L.push_back(i);
	}
	deque<deque<int> > P = genPerms(L);
	for (size_t i = 0; i < P.size(); i++) {
		for (size_t j = 0; j < P[i].size(); j++) {
			cout << P[i][j] << " ";
		}
		cout << endl;
	}
}

void sortTest()
{
	const size_t n = 20;
	int A[n];
	int B[n]; // auxillary for merging.
	for (size_t i = 0; i < n; i++) {
		A[i] = rand() % 1000;
		cout << A[i] << " ";
	}
	cout << endl;
	mergeSort(A,0,n-1,B);
	for (size_t i = 0; i < n; i++) {
		cout << A[i] << " ";
	}
	cout << endl;
}

int main()
{
	sortTest();
	gcdTest();
	permTest();
	return 0;
}
