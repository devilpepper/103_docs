#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <vector>
using std::vector;

/* TODO: write a function that re-allocates an array.
 * That is, it takes a pointer, the original size, and
 * a desired new size, and then "grows" the array to the
 * new size, preserving the contents.  Be careful with the
 * prototoype (value vs reference)
 * */

int main()
{
	/* NEW CONCEPT: dynamic memory -- allocate storage as the program is
	 * running.  How to do it? In C++ we have the "new" operator.  Abstractly,
	 * it does the following things:
	 * 1. Asks OS for a block of memory
	 * 2. Once the OS has marked that memory as yours,
	 *    it gives you the address of the block.
	 * NOTE: in reality, libc is managing the system calls, so there's
	 * not a 1-1 correspondence between using "new" and talking with
	 * the kernel. */
	/* The location of the block will be on the "heap", rather than the
	 * run-time stack.  Let's see some examples: */

	int* p = new int; // this will dynamically allocate a single int.
	// we can also dynamically allocate entire arrays:
	int n;
	cout << "Enter array size: ";
	cin >> n;
	int* A = new int[n]; // A will point to a block of n ints.
	/* NOTE: in older versions of the C++ standard, the following
	 * would NOT work, since the size of the array is not constant: */
	// int B[n]; // issue: the size is not a constant.
	/* NOTE: current versions of the standard allow for this, and make
	 * their own new and delete calls behind the scenes, storing only
	 * a pointer on the run-time stack. */

	/* once we're done with the memory, *we have to let the OS know*.  how to
	 * do this?  The delete operator:  */
	delete p;
	delete[] A;
	/* NOTE: it is good practice to use the brackets for arrays, but in most
	 * cases it would work either way.  Pretty sure it is a bad idea to use
	 * brackets on non-arrays though! */

	/* TODO: without using a vector, string, or anything else from the STL,
	 * write something that reads arbitrarily many integers from stdin and
	 * then prints them out in reverse order.  Use your function above which
	 * grows an array...
	 * TODO: How often did you re-grow the array?  Analyze the total number of
	 * statements your program will have to execute if:
	 * 1. you grow the vector by 1 element (or any constant) when you run out
	 *    of space, and
	 * 2. you double the size of the array every time you run out of space. */

	return 0;
}
