#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include "vector2.h"

int example(vector2 w)
{
	cout << w[0] << endl;
	return 0;
}

/* NOTE: most of the time you will not want to pass
 * vectors and other potentially large objects by value.
 * You can "fake" a by value call by passing the parameter
 * as a constant reference, like this:
 * int example(const vector2& w);
 * */

int main()
{
	vector2 v;
	v.push_back(199);
	v.push_back(2929);
	cout << v[0] << endl << v[1] << endl;
	/* TODO: uncomment the following line which calls the example
	 * function above, and see if you can figure out what is going
	 * wrong.  It's kind of tricky. */
	cout << example(v) << endl;
	return 0;
}
